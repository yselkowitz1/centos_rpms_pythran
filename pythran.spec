Name:           pythran
Version:        0.14.0
Release:        %autorelease
Summary:        Ahead of Time Python compiler for numeric kernels

# pythran is BSD
# pythran/graph.py has bits of networkx, also BSD
# pythran/pythonic/patch/complex is MIT or NCSA
License:        BSD and (MIT or NCSA)

# see pythran/pythonic/patch/README.rst
# The version is probably somewhat around 3
Provides:       bundled(libcxx) = 3

# see pythran/graph.py
# Only bundles one function from networkx
Provides:       bundled(python3dist(networkx)) = 2.6.1


%py_provides    python3-%{name}

URL:            https://github.com/serge-sans-paille/pythran
Source0:        %{url}/archive/%{version}/%{name}-%{version}.tar.gz

# Get rid of numpy.distutils
# https://github.com/serge-sans-paille/pythran/pull/2149
Patch:          %{url}/pull/2149.patch

# there is no actual arched content
# yet we want to test on all architectures
# and we also might need to skip some
%global debug_package %{nil}

BuildRequires: make
BuildRequires:  boost-devel
BuildRequires:  flexiblas-devel
BuildRequires:  gcc-c++
BuildRequires:  pyproject-rpm-macros
BuildRequires:  python3-devel
BuildRequires:  xsimd-devel >= 8

# For docs
# Avoid building the docs by default on RHEL not to drag pandoc into RHEL:
%bcond docs %{undefined rhel}
%if %{with docs}
BuildRequires:  pandoc
%endif

# For tests
BuildRequires:  python3-pytest
BuildRequires:  /usr/bin/python
BuildRequires:  python3-scipy

# these packages are not included in RHEL
%if %{undefined rhel}
# this is used for supporting -n auto in %%pytest
BuildRequires:  python3-pytest-xdist
# this is only used to test integration with ipython
BuildRequires:  /usr/bin/ipython
%endif

# This is a package that compiles code, it runtime requires devel packages
Requires:       flexiblas-devel
Requires:       gcc-c++
Requires:       python3-devel
Requires:       boost-devel
Requires:       xsimd-devel >= 8

%description
Pythran is an ahead of time compiler for a subset of the Python language, with
a focus on scientific computing. It takes a Python module annotated with a few
interface description and turns it into a native Python module with the same
interface, but (hopefully) faster. It is meant to efficiently compile
scientific programs, and takes advantage of multi-cores and SIMD
instruction units.


%prep
%autosetup -p1 -n %{name}-%{version}
find -name '*.hpp' -exec chmod -x {} +
sed -i '1{/#!/d}' pythran/run.py

# Remove bundled header libs and use the ones from system
rm -r third_party/boost third_party/xsimd
cat >> setup.cfg << EOF
[build_py]
no_boost=True
no_xsimd=True
EOF

# Both OpenBLAS and FlexiBLAS are registered as "openblas" in numpy
sed -i 's|blas=blas|blas=openblas|' pythran/pythran-linux*.cfg
sed -i 's|libs=|libs=flexiblas|' pythran/pythran-linux*.cfg
sed -i 's|include_dirs=|include_dirs=/usr/include/flexiblas|' pythran/pythran-linux*.cfg

# not yet available in Fedora
sed -i '/guzzle_sphinx_theme/d' docs/conf.py docs/requirements.txt

# The tests have some cflags in them
# We need to adapt the flags to play nicely with other Fedora's flags
# E.g. fortify source implies at least -O1
sed -i -e 's/-O0/-O1/g' -e 's/-Werror/-w/g' pythran/tests/__init__.py


%generate_buildrequires
%pyproject_buildrequires %{?with_docs:-x doc}


%build
%pyproject_wheel

%if %{with docs}
PYTHONPATH=$PWD make -C docs html
rm -rf docs/_build/html/.{doctrees,buildinfo}
%endif


%install
%pyproject_install
%pyproject_save_files %{name} omp


%check
# https://bugzilla.redhat.com/show_bug.cgi?id=1747029#c12
k="not test_numpy_negative_binomial"
%if 0%{?__isa_bits} == 32
# These tests cause memory (address space) exhaustion; see discussion in
# https://src.fedoraproject.org/rpms/pythran/pull-request/28.
for t in test_fftn_{8,17,20,22} \
    test_{h,ih,ir}fft_{8,14} \
    test_{,i}fft_3d{,_axis,f64_axis,int64_axis} \
    test_numpy
do
  k="$k and not ${t}"
done
%endif
%ifarch aarch64
# the test is so flaky it makes the build fail almost all the time
k="$k and not test_interp_8"
%endif
%ifarch %{power64}
# https://github.com/serge-sans-paille/pythran/pull/1946#issuecomment-992460026
k="$k and not test_setup_bdist_install3"
%endif
%ifarch s390x
k="$k and not test_numpy_chisquare0a"
%endif
%if %{defined rhel}
# this test needs ipython
k="$k and not test_loadext_and_run"
%endif

# Don’t run tests in parallel on 32-bit architecutres. RRunning tests in serial
# makes memory errors, which occur in some tests on 32-bit architectures, more
# reproducible, and makes the output when they occur less confusing: we tend to
# get individual test failures rather than a pytest INTERNALERROR.
%if %{undefined rhel} && 0%{?__isa_bits} != 32
%global use_pytest_xdist 1
%endif

%pytest %{?use_pytest_xdist:-n auto} -k "$k"


%files -f %{pyproject_files}
%license LICENSE
%doc README.rst
%if %{with docs}
%doc docs/_build/html
%endif
%{_bindir}/%{name}
%{_bindir}/%{name}-config


%changelog
%autochangelog
